\selectlanguage *{english}
\contentsline {chapter}{\nonumberline Introduction}{13}{chapter*.2}%
\contentsline {chapter}{\numberline {1}State of the art}{15}{chapter.1}%
\contentsline {chapter}{\numberline {2}Theory}{17}{chapter.2}%
\contentsline {section}{\numberline {2.1}Introduction to simulations of supercooled water}{17}{section.2.1}%
\contentsline {section}{\numberline {2.2}Overview of used methods of computational geometry}{17}{section.2.2}%
\contentsline {subsection}{\nonumberline Delaunay triangulation}{17}{section*.3}%
\contentsline {subsection}{\nonumberline Periodic boundary conditions}{17}{section*.4}%
\contentsline {section}{\numberline {2.3}Overview of used structural parameters}{18}{section.2.3}%
\contentsline {subsection}{\nonumberline Tetrahedron characteristics}{18}{section*.5}%
\contentsline {subsection}{\nonumberline Local structure index}{19}{section*.6}%
\contentsline {subsection}{\nonumberline Distance between first and second shell}{20}{section*.7}%
\contentsline {section}{\numberline {2.4}R software environment and Shiny package summary}{20}{section.2.4}%
\contentsline {subsection}{\nonumberline R}{20}{section*.8}%
\contentsline {subsection}{\nonumberline Shiny}{20}{section*.9}%
\contentsline {chapter}{\numberline {3}Practice}{21}{chapter.3}%
\contentsline {section}{\numberline {3.1}Defining and processing input data}{21}{section.3.1}%
\contentsline {section}{\numberline {3.2}Structural analysis}{21}{section.3.2}%
\contentsline {subsection}{\nonumberline 3D Delaunay triangulation in periodic border criteria}{21}{section*.10}%
\contentsline {subsection}{\nonumberline Separating data into chunks}{22}{section*.11}%
\contentsline {section}{\numberline {3.3}Results visualization}{24}{section.3.3}%
\contentsline {section}{\numberline {3.4}Interactive animation}{24}{section.3.4}%
\contentsline {subsection}{\nonumberline Interactive web app}{24}{section*.12}%
\contentsline {subsection}{\nonumberline Animation}{24}{section*.13}%
\contentsline {chapter}{\numberline {4}Results evaluation}{25}{chapter.4}%
\contentsline {chapter}{\numberline {5}Conclusion}{27}{chapter.5}%
\contentsline {chapter}{\nonumberline Bibliography}{29}{chapter*.14}%
\contentsline {chapter}{\numberline {A}Contents of included CD}{35}{appendix.A}%
